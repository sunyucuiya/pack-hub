import json from '@rollup/plugin-json'; //读取json的数据
import terser from '@rollup/plugin-terser'; //最小化压缩,js压缩到一行
import resolve from "@rollup/plugin-node-resolve" //插件允许我们加载第三方模块
export default {
  input: './src/app.js', // 入口

  output: [
    {
      dir: './dist/',
      format: 'es',
    },
    // {
    //   file: './dist/bundle.min.js',
    //   format: 'iife', // 将代码封装 可以在script中使用
    //   name: 'version',// iife必须有name
    //   sourcemap:true , // 生成bundle.js.map文件，方便调试
    //   plugins: [terser()],
    // },
  ],
  external: ["lodash"], // 外部依赖的配置
  // global: {}, // 全局变量的配置
  // 插件
  plugins: [json(),resolve()],
};
