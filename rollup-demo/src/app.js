
// import util from "./components/utils";
const util = require('./components/utils')
import { cube } from "./components/math";
import {version} from '../package.json'
import print from "./components/print";
import _ from 'lodash'


import('./components/common').then(()=>common()) //动态导入

util() //commonJs

const arr1 = [1,2,3]
const arr2 = [3,2,1]
console.log('version from json!',version)

const p = document.createElement('p')
p.textContent= '立方计算结果：'+ cube(arr2[0])
document.body.appendChild(p);
print(_.join(arr1,arr2))