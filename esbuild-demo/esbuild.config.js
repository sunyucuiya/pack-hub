require('esbuild').build({
  entryPoints:['./app.jsx'],
  outfile: 'out.js',
  bundle: true,
  minify:true,
  sourcemap:true,
}).catch(() => process.exit(1))