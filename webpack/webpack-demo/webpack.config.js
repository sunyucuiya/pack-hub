const path = require('path')
// 提取css到单独的文件中 代替style-loader插入到html
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
// 静态文件更新自动引入html 以一个模板来更新
const HtmlWwebpackPlugin = require('html-webpack-plugin')
// 压缩css到一行 生产模式生效
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')
//自定义插件
const createReadmePlugin = require('./src/plugins/createReadmePlugin')

const config = {
  entry: './src/app.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].[contenthash:6].js',
    // filename: '[name].[hash:4].js',
    clean: true,
    // publicPath: './static/'
  },
  // mode: 'development',
  resolveLoader:{ //自定义loader的路径,先检索node_modules中的loader,再检索本地的loader
    modules:[
      'node_modules',
      path.resolve(__dirname,'./src/loaders') 
    ]
  },
  module: {
    rules: [
      {
        oneOf: [
          {
            test: /\.js$/,
            // exclude: /node_modules/,
            // include: path.resolve(__dirname,'./src'),
            // use: ['babel-loader'], //增强js低版本兼容 @babel/preset-env转换代码,见babel.config.js
            // loader: 'babel-loader',
            // options: {
            //   cacheDirectory: true, //开启转译缓存
            //   cacheCompression:false, //关闭缓存文件压缩
            // }
            use:[
              'console-loader',
              {
                loader: 'annotation-loader',
                options:{
                  sign:'这是通过自定义loader添加的一条注释',
                  author:'faaarii'
                }
              },
            ]
          },
          {
            test: /\.css/,
            use: [
              MiniCssExtractPlugin.loader, // 'style-loader', // css插入到html中
              'css-loader', // 解析import css
              {
                loader: 'postcss-loader', //css兼容低版本浏览器 postcss通过js转换样式，还需要配合修改package.json
                options: {
                  postcssOptions: {
                    plugins: ['postcss-preset-env'], //加强兼容性
                  },
                },
              },
            ],
          },
          {
            test: /\.less$/,
            use: [
              MiniCssExtractPlugin.loader,
              'css-loader',
              {
                loader: 'postcss-loader',
                options: {
                  postcssOptions: {
                    plugins: ['postcss-preset-env'],
                  },
                },
              },
              'less-loader',
            ],
          },
          {
            test: /\.sass$/,
            use: [
              MiniCssExtractPlugin.loader,
              'css-loader',
              {
                loader: 'postcss-loader',
                options: {
                  postcssOptions: {
                    plugins: ['postcss-preset-env'],
                  },
                },
              },
              'sass-loader',
            ],
          },
          {
            test: /\.(png|jpe?g|gif|svg|webp)$/,
            type: 'asset', //webpack5 新增资源类型不需要loader
            parser: {
              dataUrlCondition: {
                maxSize: 50 * 1024, //小于50kb的图片，直接转base64
              },
            },
            /**
             *generator 中的 filename 指定存放的目录
             *[hash]： 表示hash值，后面可携带自定义位数，例如[hash:8]
             *[ext]：原文件扩展名
             *[query]：添加之前的query参数
             */
            generator: {
              filename: 'static/images/[hash:6][ext][query]',
            },
          },
          {
            test: /\.(ttf|woff|woff2)$/,
            type: 'asset/resource',
            generator: {
              filename: 'static/fonts/[hash:6][ext][query]',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'static/css/app.css',
    }),
    new HtmlWwebpackPlugin({
      template: path.resolve(__dirname, 'index.html'),
    }),
    new CssMinimizerPlugin(),
    new createReadmePlugin(),
  ],
  devtool: 'source-map',
  target: 'web' ,// 部署环境，默认web
  //优化
  // optimization: {
  //   usedExports: true,
  //   moduleIds:'deterministic',
  //   runtimeChunk: 'single',//所有chunk运行时所生成的映射和一些逻辑 单独bundle
  //   splitChunks: {
  //     cacheGroups: {
  //       // 使用了node_modules中第三方库lodash，单独打包bundle
  //       vendor: {
  //         test: /[\\/]node_modules[\\/]/,
  //         name: 'vendors',
  //         chunks: 'all',
  //       },
  //     },
  //   },
  // },
  
};
/**
 * 根据mode 生成不同的配置
 */
config.mode = process.env.NODE_ENV //开发模式
if(config.mode == 'development') {
  /**
   * devServer: 启动本地http服务
   * 需要安装 webpack-dev-server 模块
   * 打包命令需要添加 serve
   * server 打包后是放在内存中的
   */
  config.devServer = {
    host: 'localhost',
    open: true, //自动打开
    port: 9000,
    hot: true,
  };
}
module.exports = config