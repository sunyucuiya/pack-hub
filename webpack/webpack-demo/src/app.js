import common from "./components/common";
import util from "./components/utils";
import { cube } from "./components/math";
// import print from "./components/print";
// import _ from 'lodash'

import './css/index.css'
import './css/index.less'
import './css/index.sass'

// import './css/iconfont.css'

common()
util()
//es6
const arr1 = [1,2,3]
const arr2 = [3,2,1]
//下面在源代码中有一条console.log()
console.log('change!')

const p = document.createElement('p')
p.textContent= '立方计算结果：'+ cube(arr2[0])
document.body.appendChild(p);
// print(_.join(arr1,arr2))