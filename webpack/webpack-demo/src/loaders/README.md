## 自定义loader
loader 是导出为一个函数的 node 模块。该函数在 loader 转换资源的时候调用。给定的函数将调用 Loader API，并通过` this` 上下文访问
### 类型
-  同步 loader
-  异步 loader
-  "Raw" Loader
- Pitching loader
### 用法准则
充分利用 loader-utils 包  
自定义步骤；
- 创建一个loaders文件夹
- 在loaders文件夹中创建一个自定义的loader文件
- 在webpack.config.js中配置loader
  - 添加配置 `resolveLoader:{modules:[path.resolve(__dirname,'./src/loaders')]}`, 详细看webpack.config.js
  - module.loaders 中添加自定义loader
- 打包测试，查看bundle.js中已经有了变化
### 手写过loader吗
有手写过，主要就是导出一个函数模块，他的参数是一个源代码字符串或者是上一个loader的处理结果，因为loader支持链式调用， 返回的是处理后的代码字符串，或者是可以被下一个loader继续处理的结果。在处理过程中，可以使用 this 上下文访问 Loader API。也可以借助 loader-utils 包来进行一些工具函数的调用。  
要使用自己本地写好的loader，需要在webpack.config.js中配置调用方式，
- 单个loader的调用 直接用path.resolve(__dirname,'./src/loaders/console-loader.js')
- 多个loader的调用 要配置resolveLoader:{modules:[path.resolve(__dirname,'./src/loaders')]}


