/**
 * 注解loader
 */
module.exports = function (source) {
  this.cacheable(); //标记当前loader可以缓存
  const loaderContext = this; //获取当前loader的上下文
  const callback = loaderContext.async(); //获取当前loader的回调函数
  const options = loaderContext.getOptions(); //获取当前loader的配置
  
  callback(null,addAnnotation(source,options.sign,options.author)); //返回注解后的代码
  return;
}

function addAnnotation(source, sign, author) {
  return `
  /** 
   * ${sign} 
   * @author: ${author}
   * @date: ${new Date().toLocaleString()}
   */
  ${source}`
}
