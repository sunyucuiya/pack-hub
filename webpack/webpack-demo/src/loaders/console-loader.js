/**
 * 删除js中的console.log(comment)
 */
module.exports = function (source) {
  this.cacheable();
  const loaderContext = this;
  const callback = loaderContext.async();
  callback(null, removeConsole(source));
  return;
}
function removeConsole(source) {
  return source.replace(/console\.log\((.*)\)/g, '');
}