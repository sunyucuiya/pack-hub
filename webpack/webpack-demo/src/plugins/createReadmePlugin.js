/**
 * 自定义插件
 * 生成readme.md文件
 * 
 * 插件导出的是一个类，包含一个apply方法，在装载的时候被调用，传入一个compiler对象(全局唯一)
 * 内部处理是用调用hooks
 */
/**
module.exports = class customPlugin {
  apply(compiler) {
    compiler.hooks.<hookName>.tap/tapAsync/tapPromise(<pluginName>, <callback>);
  }
}
*/

module.exports = class createReadmePlugin {
  constructor(options) {
    this.options = options; //配置
  }
  apply(compiler) {
    const { webpack } = compiler; //webpack实例
    const { Compilation } = webpack; //webpack编译类
    const { RawSource } = webpack.sources; //在 compilation 中表示资源的源码

    //指定一个挂载到 compilation 的钩子，回调函数的参数为 compilation 。
    compiler.hooks.compilation.tap('createReadmePlugin', compilation => {
      //通过 compilation 对象绑定各种钩子,绑定到资源处理流水线(assets processing pipeline)
      compilation.hooks.processAssets.tap(
        {
          name: 'createReadmePlugin',
          // 用某个靠后的资源处理阶段，
          // 确保所有资源已被插件添加到 compilation
          stage: Compilation.PROCESS_ASSETS_STAGE_SUMMARIZE,
        },
        assets => {
          // "assets" 是一个包含 compilation 中所有资源(assets)的对象。
          // 该对象的键是资源的路径，
          // 值是文件的源码
          let resOutput = `构建时间: ${new Date().toLocaleString()}\n`;
          resOutput += `| 文件名  | 文件大小  |\n| --------- | --------- |\n`;
          Object.entries(assets).forEach(([pathname, source]) => {
            resOutput += `| ${pathname} | ${source.size()} bytes |\n`;
          });
          //向 compilation 添加新的资源
          compilation.emitAsset('assets.md', new RawSource(resOutput));
        }
      );
    });
  }
}

