const {merge} = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common,{
  mode: 'production',
  devtool: 'source-map',
  //优化
  optimization: {
    usedExports: true,
    moduleIds:'deterministic',
    runtimeChunk: 'single',//所有chunk运行时所生成的映射和一些逻辑 单独bundle
    splitChunks: {
      cacheGroups: {
        // 使用了node_modules中第三方库lodash，单独打包bundle
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
        },
      },
    },
  }
})