const {merge} = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'source-map', //在原始代码追踪错误，
  devServer:{
    host: 'localhost',
    open: true, //自动打开
    port: 9000,
    hot: true,
  },

})